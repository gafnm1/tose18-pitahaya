# SparkList Minimal

## Intro

This is a simple application skeleton for teaching web applications in Java. We use:
-	Thymeleaf, HTML,  CSS as view technologies
- 	H2 SQL as database
-	And SparkJava as web framework

It does a simple TODO-List. What else.

## Development

### Branching
- master: the main branch for the latest release of sparklist-minimal
- master-minimalstyled: the main branch for the latest release of the styled sparklist.
- develop: head for development
- fx-abcde: feture branches


### TODO
 - Put framework (sparklist basic) into this repo in a separate branch
 - cleanup 